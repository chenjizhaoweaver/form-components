# form-components

## Introduction
Components designed to easy to use for form handlings

## Used technologies
Formik: For form related features
Yup: For validation related features
Ant Design: Basic UI components mainly used. But notify that the technologies itself can be wrapper to any UI dependencies.

## Major Concerns
* Components can provide full solution for form handlings (form data construction and sync, form validation, error handling)
* All form related configs will be centralized within form components themselves. This is to avoid scenario that configs are disassambled to both component side and a central configurations side (Usually the main form component's side). When problem occurs for this scenario, the readability is compromised due to logic separations. With all configs deployed at components' side, when a field has problem, the error location could be directly tracked to around the component.
* Except form handlings, components themselves don't contain further logic. Using the component is as easy as using original basic UI component and can be extended with business logic easily
* Ultimate easiness of use. Form, validation related params are simplified. No knowledge of the wrapped technologies required for using the components


## Major Features
* Form-components that contain both formik features and original 'Ant Design' component features
* All form configs centralized to form-components
* Support nested form property (e.g. details.email)
* Simplified 'Yup' validation schema
* Support using nested components to build a single form
