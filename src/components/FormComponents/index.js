import React from 'react';
import { useFormikContext } from 'formik';
import { Select } from 'antd';

export { default as Form } from './Form';
export { default as Input } from './Input';
export { default as Select } from './Select';
export const Option = Select.Option;
export { default as SubmitButton } from './SubmitButton';

export const useForm = useFormikContext;
export const Validation = React.createContext({});
