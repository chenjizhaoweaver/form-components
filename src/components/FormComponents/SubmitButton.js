import React from 'react';
import PropTypes from 'prop-types';
import { Button as AntButton } from 'antd';
import { useFormikContext } from 'formik';

const SubmitButton = (props) => {
  const { children, onClick, ...otherProps } = props;
  const { values } = useFormikContext();
  const onButtonClick = () => {
    onClick(values);
  };
  return (
    <AntButton onClick={onButtonClick} {...otherProps}>{children}</AntButton>
  )
};

SubmitButton.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
}

export default SubmitButton;
