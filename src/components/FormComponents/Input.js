import React, { useEffect, useContext } from 'react';
import { Field, useFormikContext } from 'formik';
import PropTypes from 'prop-types';

import { AntInput } from './AntComponents';
import { Validation } from '.';

const Input = (props) => {
  const { field, defaultValue, validation, ...otherProps } = props;
  const { setFieldValue, values } = useFormikContext();
  console.log('formik', values)
  const { addvalidation } = useContext(Validation);
  useEffect(() => {
    if(defaultValue)
      setFieldValue(field, defaultValue);
    if(validation){
      addvalidation(field, validation);
    }
  }, []);
  return <Field name={field} {...otherProps} component={AntInput} />;
}

Input.propTypes = {
  field: PropTypes.string.isRequired,
  defaultValue: PropTypes.string,
  validation: PropTypes.object,
}

export default Input;
