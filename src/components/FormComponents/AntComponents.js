import React from 'react';
import { Form, Input, Select } from 'antd';
import flatten from 'flat';

const CreateAntField = AntComponent => ({
  field,
  form,
  hasFeedback,
  label,
  submitCount,
  type,
  children,
  ...props
}) => {
  // modify here because formik 'setFieldValue' automatically flatten fields
  const touches = flatten(form.touched);
  const touched = touches[field.name];
  const submitted = submitCount > 0;
  const errors = flatten(form.errors);
  const hasError = errors[field.name];
  const submittedError = hasError && submitted;
  const touchedError = hasError && touched;
  const onInputChange = ({ target: { value } }) =>
    form.setFieldValue(field.name, value);
  const onChange = value => form.setFieldValue(field.name, value);
  const onBlur = () => form.setFieldTouched(field.name, true);
  return (
    <div className="field-container">
      <Form.Item label={label}
        hasFeedback={(hasFeedback && submitted) || (hasFeedback && touched) ? true : false}
        help={submittedError || touchedError ? hasError : false}
        validateStatus={submittedError || touchedError ? "error" : "success"}
      >
        <AntComponent {...field} {...props} onBlur={onBlur} onChange={type ? onInputChange : onChange}>
          {children}
        </AntComponent>
      </Form.Item>
    </div>
  );
};

export const AntInput = CreateAntField(Input);
export const AntSelect = CreateAntField(Select);
