import React, { useEffect, useContext } from 'react';
import { Field, useFormikContext } from 'formik';
import PropTypes from 'prop-types';

import { AntSelect } from './AntComponents';
import { Validation } from '.';

const Select = (props) => {
  const { field, defaultValue, validation, ...otherProps } = props;
  const { setFieldValue } = useFormikContext();
  const { addvalidation } = useContext(Validation);
  useEffect(() => {
    if(defaultValue)
      setFieldValue(field, defaultValue);
    if(validation){
      addvalidation(field, validation);
    }
  }, []);
  return (
    <Field name={field} {...otherProps} component={AntSelect} />
  );
}

Select.propTypes = {
  field: PropTypes.string.isRequired,
  defaultValue: PropTypes.string,
  validation: PropTypes.object,
}

export default Select;
