import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { set } from 'lodash';

import { Validation } from '.';

// for inner logic use
const usevalidation = () => {
  const [validation, setValidation] = useState();
  const addvalidation = (field, validation) => {
    setValidation((prevValidation) => {
      const newValidation = { ...prevValidation };
      set(newValidation, field, validation);
      return newValidation;
    });
  }
  return [validation, addvalidation];
}

const generateYupSchema = (validation) => (
  Yup.object().shape(Object.keys(validation).reduce((schema, key) => ({
    ...schema,
    [key]: Yup.isSchema(validation[key])?validation[key]:generateYupSchema(validation[key])
  }), {}))
);

const Form = (props) => {
  const { children, ...otherProps } = props;
  const [validation, addvalidation] = usevalidation();
  const context = { addvalidation };
  const validationSchema = validation ? generateYupSchema(validation): validation; //undefined case
  return (
    <Validation.Provider value={context}>
      <Formik initialValues={{}} validationSchema={validationSchema} {...otherProps} >
        <Fragment>
          {children}
        </Fragment>
      </Formik>
    </Validation.Provider>
  );
};

Form.propTypes = {
  children: PropTypes.node
}

export default Form;
