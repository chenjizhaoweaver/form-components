import React, { Fragment, useEffect, useCallback } from 'react';
import { hooks } from '../../store';
import { Row, Col } from 'antd';
import * as Yup from 'yup';
import { get } from 'lodash';

import { Input, Form, SubmitButton } from '../FormComponents';
import UserDetails from './UserDetails';

const UserList = () => {
  const {
    users, loading, fetchAllUsersEffect, fetchAllUsersLoading, createUserEffect, updateUserEffect, deleteUserEffect, userCount
  } = hooks.useUserModel();
  const addUser = useCallback((values) => {
    const name = get(values, 'details.name', '');
    const email = get(values, 'details.email', '');
    const gender = get(values, 'details.gender', '');
    createUserEffect({ id: values.id, name, email, gender });
  }, []);
  const editUser = useCallback((values) => {
    const name = get(values, 'details.name', '');
    const email = get(values, 'details.email', '');
    const gender = get(values, 'details.gender', '');
    updateUserEffect({ id: values.id, name, email, gender });
  }, []);
  const removeUser = useCallback((values) => {
    deleteUserEffect({ id: values.id });
  }, []);
  useEffect(() => {
    if(!fetchAllUsersLoading)
      fetchAllUsersEffect();
  }, []);
  return (
    <Fragment>
      <Form>
        <SubmitButton onClick={addUser} >Create</SubmitButton>
        <SubmitButton onClick={editUser} >Update</SubmitButton>
        <SubmitButton onClick={removeUser} >Delete</SubmitButton>
        <Row>
          <Col span={3}>
            <Input field="id" type="text" placeholder="User Id"
              validation={Yup.number().typeError('Invalid number').max(20, 'Must be less than or equal to 20').required('Required')}
            />
          </Col>
        </Row>
        <UserDetails />
      </Form>
      {loading ? "Loading...": (
        <ul>
          Number of users: {userCount}
          {
            users.map(item => (
              <li key={item.name}>Id: {item.id}; Name: {item.name}; Email: {item.email}; Gender: {item.gender}</li>
            ))
          }
        </ul>
      )}
    </Fragment>
  );
}

export default UserList;
