import React, { Fragment } from 'react';
import { Row, Col } from 'antd';
import * as Yup from 'yup';

import { Input, Select, Option } from '../FormComponents';

const GENDER = {
  MALE: {
    label: 'Male',
    value: 'male'
  },
  FEMALE: {
    label: 'Female',
    value: 'female'
  }
};

const UserDetails = () => (
  <Fragment>
    <Row>
      <Col span={3}>
        <Input field="details.name" type="text" placeholder="User Name"
          validation={Yup.string().max(10, 'Must be 10 characters or less').required('Required')}
        />
      </Col>
    </Row>
    <Row>
      <Col span={3}>
        <Input field="details.email" type="email" placeholder="User Email"
          validation={Yup.string().email('Invalid email address').required('Required')}
        />
      </Col>
    </Row>
    <Row>
      <Col span={3}>
        <Select field="details.gender" defaultValue={GENDER.MALE.value} style={{width:120}}>
          {Object.values(GENDER).map(gender => <Option key={gender.value} value={gender.value}>{gender.label}</Option>)}
        </Select>
      </Col>
    </Row>
  </Fragment>
);

export default UserDetails;
