import Axios from 'axios';
import { get } from 'lodash';

export const HTTP_METHODS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
}

const BASE_URL = `${process.env.API_PROTOCOL}://${process.env.API_HOST}:${process.env.API_PORT}/`;

const BASE_HEADERS = {
  'Content-Type': 'application/json; charset=utf-8',
  // ...other required headers. e.g. auth params
};

const axios = Axios.create({baseURL: BASE_URL});

const generateUrl = (url) => {
  if(typeof url === 'function')
    return url;
  else {
    const urlFn = () => url.toString();
    return urlFn;
  }
}

export const generateClient = (httpMethod, url) => {
  if(!HTTP_METHODS[httpMethod])
    throw `Http method not found: ${httpMethod}`;
  const urlFn = generateUrl(url);
  const client = (reqParams) => {
    const pathParams = get(reqParams, 'pathParams');
    const headerParams = get(reqParams, 'headerParams');
    const queryParams = get(reqParams, 'queryParams');
    const bodyParams = get(reqParams, 'bodyParams');
    const strUrl = urlFn(pathParams);
    return axios.request({
      url: strUrl,
      method: httpMethod,
      headers: {...BASE_HEADERS, ...headerParams},
      params: queryParams,
      data: bodyParams,
    }).then(resp => resp.data)
    .catch(err => {
      console.log('error', JSON.stringify(err));
    })
  };
  return client;
}
