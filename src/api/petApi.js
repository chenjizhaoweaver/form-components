import { generateClient, HTTP_METHODS } from './axios';

const PETS = 'pets';

export async function fetchAllPets(userId) {
  const client = generateClient(HTTP_METHODS.GET, PETS);
  const reqParams = userId ? {queryParams:{id:1}} : undefined;
  return await client(reqParams);
}
