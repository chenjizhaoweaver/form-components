import { generateClient, HTTP_METHODS } from './axios';

const USERS = 'users';
const findUserById = (pathParams) => `${USERS}/${pathParams.id}`;

export async function fetchAllUsers() {
  const client = generateClient(HTTP_METHODS.GET, USERS);
  return await client();
}

export async function createUser(user) {
  const client = generateClient(HTTP_METHODS.POST, USERS);
  return await client({ bodyParams: user });
}

export async function updateUser(user) {
  const client = generateClient(HTTP_METHODS.PUT, findUserById);
  return await client({
    pathParams: {id:user.id},
    bodyParams: user
  });
}

export async function deleteUser(userId) {
  const client = generateClient(HTTP_METHODS.DELETE, findUserById);
  await client({ pathParams: {id:userId} });
  return userId;
}
