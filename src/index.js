import React from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader/root';

import App from './App';

import 'antd/dist/antd.css';

// only apply deployment level contents
const Component = hot(App);
ReactDOM.render(
  <Component />,
  document.getElementById(process.env.REACT_DOM_ELEMENT)
);
