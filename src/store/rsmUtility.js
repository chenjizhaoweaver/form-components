import { init } from '@rematch/core';
import selectPlugin from '@rematch/select';
import createLoadingPlugin from '@rematch/loading';

import { useSelector as useReduxSelector, shallowEqual } from 'react-redux';
import { camelCase, intersection } from 'lodash';

export function useSelector(selector) {
  return useReduxSelector(selector, shallowEqual)
}

const identifiedCheck = (targetA, targetB, identifier) => typeof identifier === 'function' ?
  (identifier(targetA) === identifier(targetB)) : (targetA[identifier] === targetB[identifier]);

/**
 * set reducer is for object data;
 * add/edit/remove reducers are mainly for array data, but still has simple logic for handling object data
 * addReducer logic:
 *  If original state is an array, insert payload at bottom
 *  Otherwise create an array with the original value as first element and paylad as second element
 *  If payload contain more than one element, handle each of them with above steps
 * editReducer logic:
 *  If original state is an array, edit respective element
 *  Otherwise create an array with the original value and override the original payload with new payload
 *  If payload contain more than one element, handle each of them with above steps
 * removeReducer lgoic:
 *  If original state is an array, remove respective element
 *  Otherwise leave with an empty array
 *  If payload contain more than one element, handle each of them with above steps
 */
const generateModels = (models, configs) => {
  for(let model of Object.values(models)) {
    if(model.state && configs.autoReducer){
      const reducers = Object.keys(model.state).reduce((reducers, key) => ({
        ...reducers,
        [camelCase(`set_${key}`)]: (state, payload) => ({ ...state, [key]: payload }),
        [camelCase(`add_${key}`)]: (state, payload) => {
          const prevData = Array.isArray(state[key]) ? state[key] : [state[key]];
          const newData = Array.isArray(payload) ? payload : [payload];
          const data = [...prevData, ...newData];
          return {...state, [key]: data};
        },
        [camelCase(`edit_${key}`)]: (state, payload, identifier) => {
          const prevData = Array.isArray(state[key]) ? state[key] : [state[key]];
          const updateData = Array.isArray(payload) ? payload : [payload];
          const data = prevData.map(item => updateData.find(updateItem => identifiedCheck(item, updateItem, identifier)) || item);
          return {...state, [key]: data};
        },
        [camelCase(`remove_${key}`)]: (state, payload, identifier) => {
          const prevData = Array.isArray(state[key]) ? state[key] : [state[key]];
          const removeData = Array.isArray(payload) ? payload : [payload];
          const data = prevData.filter(item => !removeData.find(removeItem => identifiedCheck(item, removeItem, identifier)));
          return {...state, [key]: data};
        }
      }), {});
      model.reducers = {
        ...model.reducers,
        ...reducers,
      }
    }
  }
  return models;
};
//({ ...state, [key]: state[key].filter(item => item[identifier] !== payload[identifier]) })
export const generateStore = (models, configs) => {
  const store = init({
    models: generateModels(models, configs),
    plugins: [selectPlugin(), createLoadingPlugin({})]
  });
  return store;
}

export const generateHooks = (models, store) => Object.values(models).reduce((hooks, model) => ({
  ...hooks,
  [camelCase(`use_${model.name}`)]: () => {
    const checkItems = [model.state, model.selectors, model.reducers, model.effects?model.effects():undefined]
      .filter(item => item).map(item => Object.keys(item));
    const dupNames = intersection(...checkItems, []/* Case there is only one valid property */);
    if(dupNames.length)
      throw `Some names duplicates in model ${model.name}: ${dupNames.join(';')}`;
    const { dispatch, select } = store;
    // original state
    const states = model.state ? Object.keys(model.state).reduce((states, key) => ({
      ...states,
      [key]: useSelector(state => state[model.name][key])
    }), {}): {};
    // selectors
    const selectors = model.selectors ? Object.keys(model.selectors).reduce((selectors, selectorName) => ({
      ...selectors,
      [selectorName]: useSelector(select[model.name][selectorName])
    }), {}): {};
    // reducers
    const reducers = model.reducers ? Object.keys(model.reducers).reduce((reducers, reducerName) => ({
      ...reducers,
      [reducerName]: (payload) => {dispatch[model.name][reducerName](payload)},
    }), {}): {};
    // effects and effect loading
    const effects = model.effects ? Object.keys(model.effects()).reduce((effects, effectName) => ({
      ...effects,
      [effectName]: (payload) => {dispatch[model.name][effectName](payload)},
      [`${effectName}Loading`]: useSelector(state => state.loading.effects[model.name][effectName]),
    }), {}): {};
    // overall loading
    const loading = model.effects ? {loading: useSelector(state => state.loading.models[model.name])} : loading;
    return {
      ...states,
      ...selectors,
      ...reducers,
      ...effects,
      ...loading
    };
  }
}), {});
