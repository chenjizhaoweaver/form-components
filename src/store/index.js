import userModel from './user/model';

import { generateStore, generateHooks } from './rsmUtility';

const models = {
  userModel,
};

const modelConfigs = {
  autoReducer: true,
};

const store = generateStore(models, modelConfigs);
export const { getState, dispatch, select } = store;
export default store;
export const hooks = generateHooks(models, store);
