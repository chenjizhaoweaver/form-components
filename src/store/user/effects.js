import * as userApi from '../../api/userApi';

export async function fetchAllUsersEffect() {
  const users = await userApi.fetchAllUsers();
  this.setUsers(users);
}

export async function createUserEffect(user) {
  const newUser = await userApi.createUser(user);
  this.addUsers(newUser);
}

export async function updateUserEffect(user) {
  const newUser = await userApi.updateUser(user);
  this.editUsers(newUser, 'id');
}

export async function deleteUserEffect(user) {
  await userApi.deleteUser(user.id);
  this.removeUsers(user, 'id');
}
