import * as effects from './effects';
import * as selectors from './selectors';

export default {
  name: 'userModel',
  state: {users: []},
  effects: () => effects,
  selectors: Object.keys(selectors).reduce((selections, selectorName) => ({
    ...selections,
    [selectorName]: () => selectors[selectorName]
  }), {}),
};
