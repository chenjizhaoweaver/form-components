import { get } from 'lodash';

export const userCount = state => get(state, 'userModel.users', []).length;
