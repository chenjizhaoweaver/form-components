const path = require('path');
const webpack = require('webpack');

module.exports = (envVars, dirname) => ({
  mode: 'development',
  resolve: {
    alias: { 'react-dom': '@hot-loader/react-dom' }
  },
  entry: {
    main: ['react-hot-loader/patch', 'index.js']
  },
  devtool: 'eval-source-map',
  output: {
    path: path.join(dirname, '/build'),
    filename: '[name].bundle.js',
    publicPath: '/'
  },
  devServer: {
    contentBase: 'src',
    compress: true,
    hot: true,
    port: parseInt(envVars.PORT),
    host: envVars.HOST,
    disableHostCheck: true,
    historyApiFallback: {
      disableDotRule: true
    },
    stats: 'minimal',
    overlay: true
    // proxy: {
    //   '/api/**': {
    //     target: {
    //       port: 8080
    //     },
    //     secure: false
    //   },
    //   '/actuator/**': {
    //     target: {
    //       port: 8080
    //     },
    //     secure: false
    //   }
    // }
  },
  module: {
    rules: [
      {//dev build need to find expose bug; easy integration with eslint
        test: /\.js$/,
        exclude: /node_modules/,
        enforce: 'pre',
        use: { loader: 'eslint-loader', options: { configFile: './.eslintrc' } },
      }
    ]
  },
  plugins: [
    new webpack.EnvironmentPlugin({ ...envVars }),
    new webpack.HotModuleReplacementPlugin() //enable hot reloading
  ]
});
