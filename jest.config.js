module.exports = {
  testPathIgnorePatterns: ["/node_modules/"],
  testMatch: [ "**/?(*.)+(spec|test).[jt]s?(x)" ],
  collectCoverage: true,
  collectCoverageFrom: ["src/app/**/*.[jt]s?(x)"],
  setupFiles: ["./configs/enzyme.config.js"]
}